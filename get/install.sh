#!/bin/bash
#version: 1.0

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

version="1.0"

net_interface="eth0"
time_zone="Europe/Lisbon"
swap_memory=2
root_mail="pm@mowster.com"
ssh_port=2020
gmail="mwt.develop@gmail.com"
gmail_pass=""
ignore_ip="45.79.11.229 139.162.199.161"
user="sshfs"
group="google"
user_pass=""



# _ commands
commands=( "mkswap" "swapon" "sysctl" "semanage" )
for command in "${commands[@]}"
do
	eval "${command//-/_}=$(command -v $command)"
done



# dns
echo 'interface "'$net_interface'" {' > /etc/dhcp/dhclient.conf
echo -e '\tprepend domain-name-servers 1.1.1.1, 1.0.0.1, 8.8.8.8, 8.8.4.4, 208.67.222.222, 208.67.220.220;' >> /etc/dhcp/dhclient.conf
echo '}' >> /etc/dhcp/dhclient.conf
systemctl restart network.service


# time
timedatectl set-timezone $time_zone


# swap file
dd if=/dev/zero of=/swapfile bs=1024 count=$((1024 * 1024 * $swap_memory))
chmod 600 /swapfile
$mkswap /swapfile
$swapon /swapfile
echo "/swapfile swap swap defaults 0 0" >> /etc/fstab
$sysctl vm.swappiness=10


# root mail
echo $root_mail > /root/.forward
newaliases


# update
yum install epel-release -y && yum update -y && yum install yum-utils iptables-services bind-utils nano unzip unar rclone -y
package-cleanup -y --oldkernels --count=1


# ssh
$semanage port -a -t ssh_port_t -p tcp $ssh_port 
curl -o '/etc/ssh/sshd_config' -L --compressed 'https://bitbucket.org/mowster/google/raw/a7587e52a553e42c6dded16bc0a472810e45f8a5/get/centos/7/x86_64/ssh/sshd_config' 
sed -i 's/\(^Port 22\)/Port '$ssh_port'/' /etc/ssh/sshd_config
systemctl restart sshd.service 


# bash 
curl -o '/etc/profile.d/bash.sh' -L --compressed 'https://bitbucket.org/mowster/google/raw/1d2f93d12cfa886484bae05107be9cf75fa9d524/get/centos/7/x86_64/bash.sh' 


# mail
systemctl stop sendmail.service && systemctl mask sendmail.service && yum remove sendmail -y
systemctl stop postfix.service && systemctl mask postfix.service && yum remove postfix -y
systemctl reset-failed
rm -rf /etc/mail; rm -rf /etc/postfix
yum install exim-mysql mailx -y
alternatives --set mta /usr/sbin/sendmail.exim
curl -o '/etc/exim/exim.conf' -L --compressed 'https://bitbucket.org/mowster/google/raw/a7587e52a553e42c6dded16bc0a472810e45f8a5/get/centos/7/x86_64/exim/exim.conf'
sed -i 's/\(<address@gmail.com>\)/'$gmail'/' /etc/exim/exim.conf
sed -i 's/\(<password>\)/'$gmail_pass'/' /etc/exim/exim.conf
systemctl -f enable exim.service && systemctl start exim.service


# failban
yum install fail2ban fail2ban-mail -y
systemctl -f enable fail2ban.service && systemctl start fail2ban.service
curl -o '/etc/fail2ban/jail.local' -L --compressed 'https://bitbucket.org/mowster/google/raw/a7587e52a553e42c6dded16bc0a472810e45f8a5/get/centos/7/x86_64/fail2ban/jail.local' 
curl -o '/etc/fail2ban/action.d/mail.local' -L --compressed 'https://bitbucket.org/mowster/google/raw/7c5974419dbb9ade82e7f389a9bfca69c6e3426f/get/centos/7/x86_64/fail2ban/action.d/mail.local'
sed -i "s/\(^ignoreip = \)/ignoreip = \"${ignore_ip}\"/" /etc/fail2ban/jail.local
sed -i 's/\(<ssh_port>\)/'$ssh_port'/' /etc/fail2ban/jail.local
systemctl restart fail2ban.service


# sshfs
useradd $user && groupadd $group
usermod -a -G $group $user
mkdir -p /home/$user/_tmp
echo $user:$user_pass | chpasswd
chown -R $user:$group /home/$user
